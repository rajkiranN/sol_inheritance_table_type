﻿using sol_inheritance_table_type.EF;
using sol_inheritance_table_type.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_inheritance_table_type
{
    public class EmployeeRepository : PersonRepository
    {
        //#region Declaration
        //private TestDBEntities db = null;
        //#endregion

        //#region Constructor
        //public EmployeeRepository()
        //{
        //    db = new TestDBEntities();
        //}
        //#endregion

        #region Constructor
        public EmployeeRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<EmployeeEntity>> GetEmployeeData()
        {
            try
            {
                return await Task.Run(() => {

                    var getQuery =
                        base.DbObject
                        ?.tblPersons
                        ?.OfType<TblEmployee>()
                        ?.AsEnumerable()
                        ?.Select(this.SelectEmployeeData)
                        ?.ToList();

                    return getQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<TblEmployee, EmployeeEntity> SelectEmployeeData
        {
            get
            {
                return
                    (leTblEmployeeObj) => new EmployeeEntity()
                    {
                        PersonId = leTblEmployeeObj.PersonId,
                        FirstName = leTblEmployeeObj.FirstName,
                        LastName = leTblEmployeeObj.LastName,
                        Salary = leTblEmployeeObj.Salary
                    };
            }
        }

        #endregion
    }
}
