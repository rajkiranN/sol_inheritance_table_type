﻿using sol_inheritance_table_type.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_inheritance_table_type
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<EmployeeEntity> listEmployeeObj =
                   await new EmployeeRepository().GetEmployeeData();



                IEnumerable<VendorEntity> listVendorObj =
                    await new VendorRepository().GetVendorData();

            }).Wait();
        }
    }
}
