﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_inheritance_table_type.Entity
namespace Sol_EF_Table_Type_Inheritance.Entity
{
    public class VendorEntity : PersonEntity
    {
        public decimal? Wages { get; set; }
    }
}
