﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_inheritance_table_type.Entity
{
    public class EmployeeEntity : PersonEntity
    {
        public decimal? Salary { get; set; }
    }
}
