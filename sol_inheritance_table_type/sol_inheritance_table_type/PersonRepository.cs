﻿using sol_inheritance_table_type.EF;
using sol_inheritance_table_type.Entity; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_inheritance_table_type
{
    public class PersonRepository
    {
        #region Declaration
        private TestDBEntities db = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            db = new TestDBEntities();

            this.DbObject = db;
        }
        #endregion

        #region Property
        public TestDBEntities DbObject { get; set; }
        #endregion
    }
}