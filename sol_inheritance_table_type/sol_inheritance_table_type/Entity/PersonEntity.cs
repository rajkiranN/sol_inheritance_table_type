﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sol_inheritance_table_type.Entity
{
    public class PersonEntity
    {
        #region Property
        public decimal PersonId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }
        #endregion 
    }
}